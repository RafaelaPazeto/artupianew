//
//  ExploreTableViewController.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class ExploreTableViewController: UITableViewController {

    @IBOutlet weak var artworksCollectionView: ArtworksCollectionView!
    @IBOutlet weak var inspireCollectionView: InspireCollectionView!
    @IBOutlet weak var featuredArtistsCollectionView: FeaturedArtistsCollectionView!
    @IBOutlet weak var storyCollectionView: StoryCollectionView!
    
    var paintings = [ArtObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        storyCollectionView.delegate = storyCollectionView
        storyCollectionView.dataSource = storyCollectionView
        inspireCollectionView.delegate = inspireCollectionView
        inspireCollectionView.dataSource = inspireCollectionView
        artworksCollectionView.delegate = artworksCollectionView
        artworksCollectionView.dataSource = artworksCollectionView
        featuredArtistsCollectionView.delegate = featuredArtistsCollectionView
        featuredArtistsCollectionView.dataSource = featuredArtistsCollectionView
        
        inspireCollectionView.categories = CategoryLoader().loadCategories()
        
        Service.shared.fetchArtList { (res) in
            
            switch res {
            case .success(let result):
                self.paintings = result.artObjects
            case .failure(let err):
                print("An error occurred: \(err)")
            }
            
            DispatchQueue.main.async {
                self.storyCollectionView.paintings = self.paintings
                self.artworksCollectionView.paintings = self.paintings
                
                let featuredArtists = self.paintings.filter({ $0.principalOrFirstMaker == "Renier van Thienen" })
                
                self.featuredArtistsCollectionView.featuredArtist = featuredArtists[0]
                self.featuredArtistsCollectionView.featuredArtists = featuredArtists
            }
        }
    }
}
