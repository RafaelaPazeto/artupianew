//
//  ArtResult.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import Foundation

// MARK: - ArtResult
struct ArtResult: Codable {
    var elapsedMilliseconds, count: Int
    var countFacets: CountFacets
    var artObjects: [ArtObject]
    var facets: [ArtResultFacet]
}

// MARK: - ArtObject
struct ArtObject: Codable {
    var links: Links
    var id, objectNumber, title: String
    var hasImage: Bool
    var principalOrFirstMaker, longTitle: String
    var showImage, permitDownload: Bool
    var webImage, headerImage: Image
    var productionPlaces: [ProductionPlace]
}

// MARK: - Image
struct Image: Codable {
    var guid: String
    var offsetPercentageX, offsetPercentageY, width, height: Int
    var url: String
}

// MARK: - Links
struct Links: Codable {
    var linksSelf, web: String

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case web
    }
}

enum ProductionPlace: String, Codable {
    case brussels = "Brussels"
    case utrecht = "Utrecht"
}

// MARK: - CountFacets
struct CountFacets: Codable {
    var hasimage, ondisplay: Int
}

// MARK: - ArtResultFacet
struct ArtResultFacet: Codable {
    var facets: [FacetFacet]
    var name: String
    var otherTerms, prettyName: Int
}

// MARK: - FacetFacet
struct FacetFacet: Codable {
    var key: String
    var value: Int
}
