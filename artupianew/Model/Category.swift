//
//  Category.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import Foundation

struct Category {
    let name: String
    let imageUrl: String
}

class CategoryLoader {
    
    func loadCategories() -> [Category] {
        
        let abstract = Category(name: "Abstract Art", imageUrl: ImageUrl.abstract)
        let representational = Category(name: "Representational Art", imageUrl: ImageUrl.representational)
        let figure = Category(name: "Figure Painting", imageUrl: ImageUrl.figure)
        let history = Category(name: "History Painting", imageUrl: ImageUrl.history)
        let portrait = Category(name: "Portrait Art", imageUrl: ImageUrl.portrait)
        let genre = Category(name: "Genre Painting", imageUrl: ImageUrl.genre)
        let landscape = Category(name: "Landscape Painting", imageUrl: ImageUrl.landscape)
        let stillLife = Category(name: "Still Life Painting", imageUrl: ImageUrl.stillLife)
        
        let categories = [abstract, representational, figure, history, portrait, genre, landscape, stillLife]

        return categories
    }
}
