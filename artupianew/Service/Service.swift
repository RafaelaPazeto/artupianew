//
//  Service.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import Foundation

/// Fetch data from API.
class Service {
    
    static let shared = Service()
    
    /// Fetch list of products for category.
    func fetchArtList(completion: @escaping (Result<ArtResult, Error>) -> ()) {
        
        guard let url = URL(string: "https://www.rijksmuseum.nl/api/en/collection?key=fqrSDOVY") else { return }
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            // Handle error
            if let err = err {
                completion(.failure(err))
                return
            }
            
            do {
                let paintings = try JSONDecoder().decode(ArtResult.self, from: data!)
                completion(.success(paintings))
            } catch let jsonError {
                completion(.failure(jsonError))
            }
            
            }.resume()
    }
}
