//
//  ArtworksCollectionViewCell.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class ArtworksCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var paintingImage: CachedImageView!
    
    static var identifier: String {
        return String(describing: "paintingCell")
    }
        
    var painting: ArtObject? {
        didSet {
            guard let painting = painting else { /*Handle error*/ return }
            
            paintingImage.loadImage(urlString: (painting.webImage.url))
        }
    }
}
