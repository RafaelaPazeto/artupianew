//
//  StoryCollectionViewCell.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var paintingImage: CachedImageView!
    @IBOutlet weak var paintingName: UILabel!
    @IBOutlet weak var paintingPrice: UILabel!
    @IBOutlet weak var paintingDescription: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var artistImage: UIImageView!
    
    static var identifier: String {
        return String(describing: "storyCell")
    }
    
    let randomPrice = Int.random(in: 300 ..< 999)
    
    var painting: ArtObject? {
        didSet {
            guard let painting = painting else { /*Handle error*/ return }
            
            paintingName.text = painting.title
            paintingImage.loadImage(urlString: (painting.webImage.url))
            artistName.text = painting.principalOrFirstMaker
            paintingDescription.text = painting.longTitle
            paintingPrice.text = "\(randomPrice) €"
        }
    }
}
