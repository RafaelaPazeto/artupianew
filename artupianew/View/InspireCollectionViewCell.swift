//
//  InspireCollectionViewCell.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class InspireCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: CachedImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    static var identifier: String {
        return String(describing: "inspireCell")
    }
        
    var category: Category? {
        didSet {
            guard let category = category else { /*Handle error*/ return }
            
            categoryTitle.text = category.name
            categoryImageView.loadImage(urlString: (category.imageUrl))
        }
    }
}
