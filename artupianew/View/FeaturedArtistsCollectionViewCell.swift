//
//  FeaturedArtistsCollectionViewCell.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class FeaturedArtistsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistCountry: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artworksCollectionView: SmallArtworksCollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        artworksCollectionView.delegate = artworksCollectionView
        artworksCollectionView.dataSource = artworksCollectionView
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    static var identifier: String {
        return String(describing: "featuredArtistsCell")
    }
        
    var painting: ArtObject? {
        didSet {
            guard let painting = painting else { /*Handle error*/ return }
            
            artistName.text = painting.principalOrFirstMaker
            artistCountry.text = painting.productionPlaces[0].rawValue
        }
    }
    
    var paintings = [ArtObject]() {
        didSet {
            artworksCollectionView.paintings = paintings
        }
    }
}
