//
//  StoryCollectionView.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import Foundation
import UIKit

class StoryCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var paintings = [ArtObject]() {
        didSet {
            reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paintings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCollectionViewCell.identifier, for: indexPath) as! StoryCollectionViewCell
        
        let paintingReceived = paintings[indexPath.item]
        cell.painting = paintingReceived
        return cell
    }
    
}
