//
//  FeaturedArtistsCollectionView.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import UIKit

class FeaturedArtistsCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var featuredArtists = [ArtObject]()
    
    var featuredArtist: ArtObject? {
        didSet {
            reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeaturedArtistsCollectionViewCell.identifier, for: indexPath) as! FeaturedArtistsCollectionViewCell
        
        let featuredArtistsReceived = featuredArtist
        cell.painting = featuredArtistsReceived
        cell.paintings = featuredArtists
        return cell
    }
    
}
