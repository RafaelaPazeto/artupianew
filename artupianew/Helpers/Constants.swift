//
//  Constants.swift
//  artupianew
//
//  Created by Vinicius Moreira Leal on 08/12/2019.
//  Copyright © 2019 Rafaela Pazeto. All rights reserved.
//

import Foundation

struct ImageUrl {

    static let abstract = "http://www.visual-arts-cork.com/images-modern/kandinsky-harmony.jpeg"
    static let representational = "http://www.visual-arts-cork.com/images-paint/degas-blue-1898.JPG"
    static let figure = "http://www.visual-arts-cork.com/images-pictures/saville-drawing.jpg"
    static let history = "http://www.visual-arts-cork.com/images-paintings/spark-life.JPG"
    static let portrait = "http://www.visual-arts-cork.com/images-artistic/rembrandt-portrait.jpg"
    static let genre = "http://www.visual-arts-cork.com/images-renaissance/massys-moneylenders.jpg"
    static let landscape = "http://www.visual-arts-cork.com/images-modern/turner-venice.jpeg"
    static let stillLife = "http://www.visual-arts-cork.com/images-paint/cezanne-stilllife-1879.jpg"
}
